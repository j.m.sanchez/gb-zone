## INTRODUCTION
<p/>GB-Zone is a web based client for GRIN-Global system. It was developed as an extension of GRIN-Global exploding the web services (SOAP) provided by GRIN-Global. GB-Zone provide a user-friendly interface keeping the use of GRIN-Global dataviews created from Admin-Tool and using the authentication and authorization provided by GRIN-Global.


## ARCHITECTURE
![GRIN-Global_Components](/uploads/b332f4dfa255ee140acc024ee1e93dab/GRIN-Global_Components.png)
## TECHNOLOGIES
* DevExpress ASP.NET MVC
* Bootstrap (for responsive web application)
* XML settings
* GG SOAP client

## TOOLS
* Visual Studio Community 2015 
* Git for Windows
* GitFlow for Visual Studio 2015
* GitLab Extension for Visual Studio

## XML Configuration
* Connection to multiple GG instances 