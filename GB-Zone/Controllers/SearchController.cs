﻿using System;
using System.Web.Mvc;
using GrinGlobal.Zone.Classes;

namespace GrinGlobal.Zone.Controllers
{
    public class SearchController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SearchController));

        /// <summary>
        /// Show empty view 
        /// </summary>
        /// <param name="moduleId"></param>
        /// <param name="formId"></param>
        /// <returns></returns>
        public ActionResult Index(string moduleId, string formId)
        {
            ViewData["moduleId"] = moduleId;
            ViewData["formId"] = formId;

            return View();
        }

        /// <summary>
        /// Get data from form parameters
        /// </summary>
        /// <param name="formdata"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index(FormCollection formdata)
        {
            DataViewsSearch search = new DataViewsSearch();

            string serverId = Session["server"].ToString();
            string moduleId = formdata["moduleId"];
            string formId = formdata["formId"];
            string fieldId = formdata["radios"];
            string value = formdata[String.Format("text {0}", fieldId)];
            
            ViewData["server"] = serverId;
            ViewData["moduleId"] = moduleId;
            ViewData["formId"] = formId;
            ViewData["viewName"] = fieldId;
            ViewData["value"] = value;
            
            return View(search.GetData(serverId, moduleId, formId, fieldId, value));
        }

        /// <summary>
        /// Load Index with populated GridView component
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="moduleId"></param>
        /// <param name="formId"></param>
        /// <param name="fieldId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ActionResult Index2(string serverId, string moduleId, string formId, string fieldId, string value)
        {
            DataViewsSearch search = new DataViewsSearch();

            ViewData["server"] = serverId;
            ViewData["moduleId"] = moduleId;
            ViewData["formId"] = formId;
            ViewData["viewName"] = fieldId;
            ViewData["value"] = value;

            return View("Index", search.GetData(serverId, moduleId, formId, fieldId, value));
        }

        /// <summary>
        /// PartialView of the GridView component
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="moduleId"></param>
        /// <param name="formId"></param>
        /// <param name="fieldId"></param>
        /// <param name="value"></param>
        /// <param name="viewName"></param>
        /// <returns></returns>
        public ActionResult GridView(string serverId, string moduleId, string formId, string fieldId, string value, string viewName)
        {
            DataViewsSearch search = new DataViewsSearch();

            ViewData["server"] = serverId;
            ViewData["moduleId"] = moduleId;
            ViewData["formId"] = formId;
            ViewData["viewName"] = fieldId;
            ViewData["value"] = value;

            ViewData["germplasmDbId"] = 0;

            return PartialView(viewName, search.GetData(serverId, moduleId, formId, fieldId, value));
        }

        /// <summary>
        /// Update data from GridView to GRIN-Global server
        /// </summary>
        /// <param name="serverId"></param>
        /// <param name="moduleId"></param>
        /// <param name="formId"></param>
        /// <param name="fieldId"></param>
        /// <param name="value"></param>
        /// <param name="viewName"></param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public ActionResult GridViewPartialUpdate(string serverId, string moduleId, string formId, string fieldId, string value, string viewName)
        {
            DataViewsSearch search = new DataViewsSearch();

            ViewData["server"] = serverId;
            ViewData["moduleId"] = moduleId;
            ViewData["formId"] = formId;
            ViewData["viewName"] = fieldId;
            ViewData["value"] = value;

            try
            {
                search.SaveData(serverId, moduleId, formId, fieldId, value);
            }
            catch (Exception e)
            {
                Guid d = Guid.NewGuid();
                log.Fatal(Guid.NewGuid(),e);
                
                ViewData["EditError"] = String.Format(e.Message);
            }

            return PartialView(viewName, search.GetData(serverId, moduleId, formId, fieldId, value));
        }
    }
}